/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Chain, Dns, HttpClient, Interceptor, Logger, Request, Response, TimeUnit, Utils } from '@ohos/httpclient';
import connection from '@ohos.net.connection';
import resmgr from '@ohos.resourceManager';
import { Utils as UtilCA } from '../utils/Utils'
import { BusinessError } from '@ohos/httpclient/src/main/ets/http';

@Entry
@Component
struct dns {
  /**
   * 配置类
   *
   * 额外提供三个测试域名
   * @State url: string = 'https://mail.qq.com';
   * @State url: string = 'https://news.qq.com';
   * @State url: string = 'http://hshapp.ncn.com.cn/wisdom3/config/config.do';
   * https://www.wanandroid.com对应的dns为https://47.104.74.169
   * 对应证书目录：httpclient-master\entry\src\main\resources\rawfile
   */
  @State url: string = 'https://www.wanandroid.com/hotkey/json';
  @State ipUrl: string = 'https://47.104.74.169/hotkey/json';
  @State jdUrl: string = 'https://www.jd.com';
  @State baiduUrl: string = 'https://www.baidu.com';
  @State urlTest: string = 'http://139.9.217.131:8001';
  @State result: string = this.getResourceString($r('app.string.Response_results'));
  scroller: Scroller = new Scroller();
  hereResourceManager: resmgr.ResourceManager = getContext().resourceManager;
  TEST_CA = 'wanandroidRoot.crt';
  TEST_OTHER_CA = 'wanandroidRSA.crt';
  BAIDU_TEST_CA = 'BaiduGlobalSign.crt';
  BAIDU_OTHER_CA = 'BaiduSSLCA.crt';

  getResourceString(res: Resource) {
    return getContext().resourceManager.getStringSync(res.id)
  }

  build() {
    Column() {
      Row() {
        Navigator({
          target: '',
          type: NavigationType.Back
        }) {
          Text($r('app.string.BACK'))
            .fontSize(10)
            .border({
              width: 1
            })
            .padding(10)
            .fontColor(0x000000)
            .borderColor(0x317aff)
        }
      }
      .height('10%')
      .width('100%')

      //输入对应的Url，这里默认为“'https://www.wanandroid.com/hotkey/json'
      TextInput({ text: this.url, placeholder: $r('app.string.Please_URL') })
        .placeholderColor('#ffffff')
        .caretColor(Color.Blue)
        .height('150px')
        .fontSize('18fp')
        .onChange((value: string) => {
          this.url = value;
        })

      Button($r('app.string.Request_Connectivity'))
        .width('80%')
        .height('80px')
        .fontSize(18)
        .fontColor(0xCCCCCC)
        .align(Alignment.Center)
        .margin(10)
        .onClick(async (event: ClickEvent) => {
          Logger.info('DNSTEST HttpClient begin');
          let client: HttpClient = new HttpClient
            .Builder()
            .dns(new CustomDns())
            .setConnectTimeout(10, TimeUnit.SECONDS)
            .setReadTimeout(10, TimeUnit.SECONDS)
            .build();
          Logger.info('DNSTEST HttpClient end');
          let context: Context = getContext();
          //获取CA证书
          let CA: string = await new UtilCA().getCA(this.TEST_CA, context);
          let otherCA: string = await new UtilCA().getCA(this.TEST_OTHER_CA, context);
          Logger.info('DNSTEST request begin');
          let request: Request = new Request.Builder()
            .url(this.url)
            .method('GET')
            .ca([CA, otherCA])
            .build();
          Logger.info('DNSTEST request end');
          //异步发送网络请求
          client.newCall(request).enqueue((result: Response) => {
            this.result = this.getResourceString($r('app.string.Response_result_1')) + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4)
            Logger.info('dns---success---' + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4));
          }, (err: BusinessError) => {
            this.result = this.getResourceString($r('app.string.Response_result_1')) + JSON.stringify(err)
            Logger.info('dns---failed---', JSON.stringify(err));
          });
        })

      //在原网络地址中，新增8.8.8.8地址
      Button($r('app.string.Request_Pass'))
        .width('80%')
        .height('80px')
        .fontSize(18)
        .fontColor(0xCCCCCC)
        .align(Alignment.Center)
        .margin(10)
        .onClick(async (event: ClickEvent) => {
          //构建http链接
          Logger.info('DNSTEST2 HttpClient begin');
          let client: HttpClient = new HttpClient
            .Builder()
            .dns(new CustomAddDns())
            .setConnectTimeout(10, TimeUnit.SECONDS)
            .setReadTimeout(10, TimeUnit.SECONDS)
            .build();
          Logger.info('DNSTEST2 HttpClient end');
          let context: Context = getContext();
          let CA: string = await new UtilCA().getCA(this.TEST_CA, context);
          let otherCA: string = await new UtilCA().getCA(this.TEST_OTHER_CA, context);
          Logger.info('DNSTEST2 request begin');
          let request: Request = new Request.Builder()
            .url(this.url)
            .method('GET')
            .ca([CA, otherCA])
            .build();
          Logger.info('DNSTEST2 request end');
          client.newCall(request).enqueue((result: Response) => {
            this.result = this.getResourceString($r('app.string.Response_result_2')) + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4)
            Logger.info('dns2---success---' + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4));
          }, (err: BusinessError) => {
            this.result = this.getResourceString($r('app.string.Response_result_2')) + JSON.stringify(err)
            Logger.info('dns2---failed---', JSON.stringify(err));
          });
        })

      //传入默认的域名，然后重定义到百度
      Button($r('app.string.Request_Baidu'))
        .width('80%')
        .height('80px')
        .fontSize(18)
        .fontColor(0xCCCCCC)
        .align(Alignment.Center)
        .margin(10)
        .onClick(async (event: ClickEvent) => {
          //构建http链接
          Logger.info('DNSTEST3  HttpClient begin');
          let client: HttpClient = new HttpClient
            .Builder()
            .dns(new CustomChangeDns())
            .setConnectTimeout(10, TimeUnit.SECONDS)
            .setReadTimeout(10, TimeUnit.SECONDS)
            .build();
          Logger.info('DNSTEST3 HttpClient end');
          let context: Context = getContext();
          let CA: string = await new UtilCA().getCA(this.BAIDU_TEST_CA, context);
          let otherCA: string = await new UtilCA().getCA(this.BAIDU_OTHER_CA, context);
          Logger.info('DNSTEST3 request begin');
          let request: Request = new Request.Builder()
            .url(this.baiduUrl)
            .method('GET')
            .ca([CA, otherCA])
            .build();
          Logger.info('DNSTEST3 request end');
          client.newCall(request).enqueue((result: Response) => {
            this.result = this.getResourceString($r('app.string.Response_result_3')) + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4)
            Logger.info('dns---success---' + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4));
          }, (err: BusinessError) => {
            this.result = this.getResourceString($r('app.string.Response_result_3')) + JSON.stringify(err)
            Logger.info('dns---failed--- ', JSON.stringify(err));
          });
        })

      //通过传入错误证书证书，报错2303501
      //百度{'address':'153.3.238.102','family':1,'port':0}
      Button($r('app.string.Request_method_4'))
        .width('80%')
        .height('80px')
        .fontSize(18)
        .fontColor(0xCCCCCC)
        .align(Alignment.Center)
        .margin(10)
        .onClick(async (event: ClickEvent) => {
          //构建http链接
          this.url = this.baiduUrl;
          Logger.info('DNSTEST4 HttpClient begin new url', this.url);
          Logger.info('DNSTEST4 HttpClient begin');
          let client: HttpClient = new HttpClient
            .Builder()
            .dns(new CustomConnectDns())
            .setConnectTimeout(10, TimeUnit.SECONDS)
            .setReadTimeout(10, TimeUnit.SECONDS)
            .build();
          Logger.info('DNSTEST4 HttpClient end');
          let context: Context = getContext();
          let CA: string = await new UtilCA().getCA(this.TEST_CA, context);
          let otherCA: string = await new UtilCA().getCA(this.TEST_OTHER_CA, context);
          Logger.info('DNSTEST4 request begin');
          let request: Request = new Request.Builder()
            .url(this.baiduUrl)
            .method('GET')
            .ca([CA, otherCA])
            .build();
          Logger.info('DNSTEST request end');
          client.newCall(request).enqueue((result: Response) => {
            this.result = this.getResourceString($r('app.string.Response_result_4')) + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4)
            Logger.info('dns---success---' + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4));
          }, (err: BusinessError) => {
            this.result = this.getResourceString($r('app.string.Response_result_4')) + JSON.stringify(err)
            Logger.info('dns---failed---', JSON.stringify(err));
          });
        })

      //传入空的dns地址，抛出错误
      Button($r('app.string.Request_DNS_Method'))
        .width('80%')
        .height('80px')
        .fontSize(18)
        .fontColor(0xCCCCCC)
        .align(Alignment.Center)
        .margin(10)
        .onClick(async (event: ClickEvent) => {
          //构建http链接
          Logger.info('DNSTEST5 HttpClient begin');
          let client: HttpClient = new HttpClient
            .Builder()
            .dns(new CustomEmptyDns())
            .setConnectTimeout(10, TimeUnit.SECONDS)
            .setReadTimeout(10, TimeUnit.SECONDS)
            .build();
          Logger.info('DNSTEST5 HttpClient end');
          let context: Context = getContext();
          let CA: string = await new UtilCA().getCA(this.TEST_CA, context);
          let otherCA: string = await new UtilCA().getCA(this.TEST_OTHER_CA, context);
          Logger.info('DNSTEST5 request begin');
          let request: Request = new Request.Builder()
            .url(this.baiduUrl)
            .method('GET')
            .ca([CA, otherCA])
            .build();
          Logger.info('DNSTEST5 request end');
          client.newCall(request).enqueue((result: Response) => {
            this.result = this.getResourceString($r('app.string.Response_result_5')) + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4)
            Logger.info('dns---success---' + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4));
          }, (err: BusinessError) => {
            this.result = this.getResourceString($r('app.string.Response_result_5')) + JSON.stringify(err)
            Logger.info('dns---failed---', JSON.stringify(err));
          });
        })

      //需要对应'https://www.wanandroid.com/hotkey/json';
      Button($r('app.string.Interceptor_method'))
        .width('80%')
        .height('80px')
        .fontSize(18)
        .fontColor(0xCCCCCC)
        .align(Alignment.Center)
        .margin(10)
        .onClick(async (event: ClickEvent) => {
          let client: HttpClient = new HttpClient
            .Builder()
            .addInterceptor(new CustomInterceptor())
            .setConnectTimeout(10, TimeUnit.SECONDS)
            .setReadTimeout(10, TimeUnit.SECONDS)
            .build();
          let context: Context = getContext();
          let CA: string = await new UtilCA().getCA(this.TEST_CA, context);
          let otherCA: string = await new UtilCA().getCA(this.TEST_OTHER_CA, context);
          let request: Request = new Request.Builder()
            .url(this.url)
            .method('GET')
            .ca([CA, otherCA])
            .build();
          client.newCall(request).enqueue((result: Response) => {
            this.result = this.getResourceString($r('app.string.Response_results')) + '\r\n' + JSON.stringify(result.result)
            Logger.info('interceptor---success---' + JSON.stringify(result.result));
          }, (err: BusinessError) => {
            this.result = this.getResourceString($r('app.string.Response_results')) + '\r\n' + JSON.stringify(err)
            Logger.info('interceptor---failed---', JSON.stringify(err));
          });
        })

      Button($r('app.string.clear'))
        .width('80%')
        .height('80px')
        .fontSize(18)
        .fontColor(0xCCCCCC)
        .align(Alignment.Center)
        .margin(10)
        .onClick(async (event: ClickEvent) => {
          this.clear();
        })
      Scroll() {
        Column() {
          Text(this.result)
            .width('80%')
            .fontSize('18fp')
            .margin(10)
        }
      }
      .width('100%')
      .layoutWeight(1)
    }
    .width('100%')
    .height('100%')
  }

  clear() {
    this.result = '';
  }
}

/**
 * 自定义CustomDns实现dns
 *
 * 只对域名进行解析
 * @netAddress 网络地址，里面携带了dns
 * @err 错误值
 */
export class CustomDns implements Dns {
  lookup(hostname: string): Promise<Array<connection.NetAddress>> {
    Logger.info('DNSTEST CustomDns begin here');
    return new Promise((resolve, reject) => {
      connection.getAddressesByName(hostname).then((netAddress) => {
        //解析出来的网址|1是Ipv4，2是IPV6|端口号
        Logger.info('DNSTEST netAddress = ' + JSON.stringify(netAddress))
        resolve(netAddress)
        Logger.info('DNSTEST CustomDns end');
      }).catch((err: BusinessError) => {
        reject(err)
      });
    })
  }
}

/**
 * 自定义CustomAddDns实现dns
 *
 * 进行解析，并且传入自定义dns
 * @netAddress 网络地址，里面携带了dns
 * @err 错误值
 */
export class CustomAddDns implements Dns {
  lookup(hostname: string): Promise<Array<connection.NetAddress>> {
    Logger.info('DNSTEST2 CustomDns2 begin here');
    return new Promise((resolve, reject) => {
      connection.getAddressesByName(hostname).then((netAddress) => {
        Logger.info('DNSTEST2 netAddress = ' + JSON.stringify(netAddress));
        if (netAddress) {
          //自定义，传入一个网络地址（两种都行）
          netAddress.push({ 'address': '8.8.8.8', 'family': 1, 'port': 0 });
          netAddress.push({ 'address': '9.9.9.9' });
          resolve(netAddress)
          Logger.info('DNSTEST2 new netAddress = ' + JSON.stringify(netAddress));
          Logger.info('DNSTEST2 CustomDns end');
        } else {
          // 如果netAddress为空，可以执行这里的其他逻辑
          Logger.error('DNSTEST2 netAddress is empty');
          let defaultNetAddress: Array<connection.NetAddress> = [{
            'address': '153.3.238.102',
            'family': 1,
            'port': 443
          }];
          resolve(defaultNetAddress);
        }
      }).catch((err: BusinessError) => {
        reject(err)
      });
    })
  }
}

/**
 * 自定义CustomChangeDns实现dns
 *
 * 进行解析，并且传入将其dns重定向到百度
 * @netAddress 网络地址，里面携带了dns
 * @err 错误值
 */
export class CustomChangeDns implements Dns {
  lookup(hostname: string): Promise<Array<connection.NetAddress>> {
    Logger.info('DNSTEST3  CustomDns  begin here ');
    return new Promise((resolve, reject) => {
      connection.getAddressesByName(hostname).then((netAddress) => {
        Logger.info('DNSTEST3 netAddress = ' + JSON.stringify(netAddress))
        if (netAddress) {
          //重定义到百度地址
          netAddress = [{ 'address': '153.3.238.102', 'family': 1, 'port': 0 }];
          Logger.info('DNSTEST3  new netAddress = ' + JSON.stringify(netAddress))
          resolve(netAddress)
          Logger.info('DNSTEST3  CustomDns end');
        } else {
          // 如果netAddress为空，可以执行这里的其他逻辑
          Logger.error('DNSTEST3 netAddress is empty');
          let defaultNetAddress: Array<connection.NetAddress> = [{
            'address': '153.3.238.102',
            'family': 1,
            'port': 443
          }];
          resolve(defaultNetAddress);
        }
      }).catch((err: BusinessError) => {
        reject(err)
      });
    })
  }
}

/**
 * 自定义CustomConnectDns实现dns
 *
 * 模拟异常场景，加入解析有问题，将dns取默认
 * @netAddress 网络地址，里面携带了dns
 * @err 错误值
 */
export class CustomConnectDns implements Dns {
  lookup(hostname: string): Promise<Array<connection.NetAddress>> {
    Logger.info('DNSTEST4  CustomDns  begin here ');
    return new Promise((resolve, reject) => {
      connection.getAddressesByName(hostname).then((netAddress) => {
        Logger.info('DNSTEST4 netAddress = ' + JSON.stringify(netAddress))
        if (netAddress) {
          //模拟异常场景，若解析出来的dns为空，则将网站取默认
          Logger.error('netAddress is empty');
          let nonaddress: Array<connection.NetAddress> = [{
            'address': '153.3.238.102',
            'family': 1,
            'port': 443
          }];
          resolve(nonaddress);
        } else {
          resolve(netAddress)
        }
      }).catch((err: BusinessError) => {
        reject(err)
      });
    })
  }
}

/**
 * 自定义CustomEmptyDns实现dns
 *
 * 模拟异常场景，传入一个空的地址
 * @netAddress 网络地址，里面携带了dns
 * @err 错误值
 */
export class CustomEmptyDns implements Dns {
  lookup(hostname: string): Promise<Array<connection.NetAddress>> {
    Logger.info('DNSTEST5 CustomDns begin here ');
    return new Promise((resolve, reject) => {
      connection.getAddressesByName(hostname).then((netAddress) => {
        Logger.info('DNSTEST5 netAddress = ' + JSON.stringify(netAddress))
        if (netAddress) {
          // 重定义到一个空地址
          netAddress = [{ 'address': '' }];
          Logger.info('DNSTEST5 new netAddress = ' + JSON.stringify(netAddress))
          resolve(netAddress)
          Logger.info('DNSTEST5 CustomDns end');
        } else {
          // 如果netAddress为空，可以执行这里的其他逻辑
          Logger.error('DNSTEST5 netAddress is empty');
          let defaultNetAddress: Array<connection.NetAddress> = [{
            'address': '153.3.238.102',
            'family': 1,
            'port': 443
          }];
          resolve(defaultNetAddress);
        }
      }).catch((err: BusinessError) => {
        reject(err)
      });
    })
  }
}

/**
 * 自定义拦截器
 *
 * 自定义拦截器实现
 * @Response Response对象
 * @err 错误值
 */
export class CustomInterceptor implements Interceptor {
  intercept(chain: Chain): Promise<Response> {
    return new Promise<Response>((resolve, reject) => {
      let originRequest: Request = chain.requestI();
      let url: string = originRequest.url.getUrl();
      let host: string = Utils.getDomainOrIp(url)
      connection.getAddressesByName(host).then((netAddress) => {
        let newRequest = originRequest.newBuilder()
        if (!!netAddress) {
          if (Utils.isIPv6(netAddress[0].address)) {
            let ipv4Address: Array<connection.NetAddress> = [];
            let ipv6Address: Array<connection.NetAddress> = [];
            for (let i = 0, len = netAddress.length; i < len; i++) {
              if (Utils.isIPv6(netAddress[i].address)) {
                ipv6Address.push(netAddress[i])
              } else {
                ipv4Address.push(netAddress[i])
              }
            }
            netAddress = [];
            netAddress = netAddress.concat(ipv4Address).concat(ipv6Address)
          }
          url = url.replace(host, netAddress[0].address)
          newRequest.url(url)
        }
        newRequest.dnsInterceptor()
        let newResponse: Promise<Response> = chain.proceedI(newRequest.build())
        resolve(newResponse)
      }).catch((err: BusinessError) => {
        reject(err)
      });
    })
  }
}