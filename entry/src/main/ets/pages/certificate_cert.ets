/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import socket from '@ohos.net.socket';
import { HttpClient, RealTLSSocket, Request, StringUtil, TLSSocketListener, Utils } from '@ohos/httpclient';
import prompt from '@ohos.prompt';
import buffer from '@ohos.buffer';
import { BusinessError } from '@ohos/httpclient/src/main/ets/http';
import hilog from '@ohos.hilog';
import resmgr from '@ohos.resourceManager';
import { TLSSocketListenerImpl } from '../model/TLSSocketListenerImpl';
import promptAction from '@ohos.promptAction';
import { CertVerifyResult, SocketConnectError } from '../model/CertVerfiyDataStrcure';

@Entry
@Component
struct certificate_cert {
  @State status: string = ''
  @State content: string = ''
  currentALPNProtocols = ["spdy/1", "http/1.1"]
  currentPasswd = "123456"
  currentSignatureAlgorithms = "rsa_pss_rsae_sha256:ECDSA+SHA256"
  currentCipherSuites = "AES256-SHA256"
  ifUseRemoteCipherPrefer = true
  protocols = [socket.Protocol.TLSv13]
  keyRes: string = 'client_rsa_private.pem.unsecure'
  certRes: string = 'client.crt'
  caRes: string[] = ['ca.crt']
  realTlsSocet?: RealTLSSocket;
  client: HttpClient = new HttpClient
    .Builder()
    .build();
  request?: Request
  scroller: Scroller = new Scroller()
  sendStr: string = ''
  ipInput: string = '106.15.92.248'
  portInput: string = '9090'
  certificate_cert_TAG: string = "struct certificate_cert page    ";
  tlsSocketListenerImpl: TLSSocketListenerImpl = new TLSSocketListenerImpl();

  getResourceString(res: Resource) {
    return getContext().resourceManager.getStringSync(res.id)
  }

  build() {
    Column() {
      Flex({
        direction: FlexDirection.Column
      }) {
        Navigator({
          target: "",
          type: NavigationType.Back
        }) {
          Text($r('app.string.BACK'))
            .fontSize(12)
            .border({
              width: 1
            })
            .padding(10)
            .fontColor(0x000000)
            .borderColor(0x317aff)
        }
      }
      .height('10%')
      .width('100%')
      .padding(10)

      Flex({
        direction: FlexDirection.Column
      }) {
        Row() {
          CertVerifyButtonStyle({
            name: $r('app.string.Load_certificate')
          })
            .onClick(() => {
              prompt.showToast({ message: this.getResourceString($r('app.string.Load_certificate')) })
              this.initTLSSocketData(true);
            })
        }.margin({ bottom: px2vp(20) })

        Row() {
          TextInput({ text: this.ipInput, placeholder: $r('app.string.Enter_server_IP') })
            .width('40%')
            .height('100%')
            .fontSize(18)
            .onChange((value: string) => {
              this.ipInput = value
            })
          Blank().width(px2vp(20))
          TextInput({ text: this.portInput, placeholder: $r('app.string.Enter_port') })
            .width('30%')
            .height('100%')
            .fontSize(18)
            .onChange((value: string) => {
              this.portInput = value
            })

          CertVerifyButtonStyle({
            name: $r('app.string.Connect_to_server'),
            tlsSocketListenerImpl: this.tlsSocketListenerImpl
          })
            .onClick(() => {
              if (this.realTlsSocet != undefined) {
                this.realTlsSocet.setVerify(true)
                this.connect();
                this.content = "";
                setTimeout(() => {
                  this.refreshData();
                }, 1000)
              }
            })

        }.margin({ top: px2vp(20) }).height(px2vp(150))

        Row() {
          TextInput({ text: this.sendStr, placeholder: $r('app.string.Enter_sent') })
            .width('60%')
            .height('100%')
            .fontSize(18)
            .onChange((value: string) => {
              this.sendStr = value
            })

          CertVerifyButtonStyle({
            name: $r('app.string.send_data'),
            tlsSocketListenerImpl: this.tlsSocketListenerImpl
          })
            .onClick(() => {
              if (this.realTlsSocet != undefined) {
                if (StringUtil.isEmpty(this.sendStr)) {
                  prompt.showToast({ message: this.getResourceString($r('app.string.Enter_sent')) })
                  return
                } else {
                  this.realTlsSocet.setVerify(false)
                  this.send();
                  setTimeout(() => {
                    if (this.tlsSocketListenerImpl.errorMsg.code != 0) {
                      this.content += "fail in send" + this.sendStr;
                    }
                  }, 500)
                }
              }
            })
        }.margin({ top: px2vp(20) }).height(px2vp(150))
      }
      .height('40%')
      .width('100%')
      .padding(10)

      Scroll(this.scroller) {
        Column() {
          Text(this.content).fontSize(18).fontColor(Color.Black).width('100%').textAlign(TextAlign.Start)
          Blank().height(20)
        }.width('100%')

      }
      .scrollable(ScrollDirection.Vertical)
      .scrollBar(BarState.On)
      .scrollBarColor(Color.Gray)
      .layoutWeight(1)

    }.width('100%').margin({
      top: 5,
      bottom: 100
    }).height('100%')
  }

  aboutToAppear() {
    this.initRealTLSSocket();
    this.request = new Request.Builder()
      .setTlsRequst(this.realTlsSocet)
      .url(this.getUrl())
      .build();
  }

  initRealTLSSocket() {
    let that = this
    this.realTlsSocet = new RealTLSSocket()
    this.realTlsSocet.setLisenter(this.tlsSocketListenerImpl)
    this.initTLSSocketData(false);
  }

  initTLSSocketData(isNeedShow: boolean) {
    let hereResourceManager: resmgr.ResourceManager = getContext().resourceManager;
    this.content = '';
    if (this.realTlsSocet != undefined) {
      this.realTlsSocet.setKeyDataByRes(hereResourceManager, this.keyRes, (errKey: BusinessError<string> | null, resultKey: Uint8Array) => {
        if (isNeedShow) {
          this.content += '\ntlsSoket key:' + JSON.stringify(resultKey) + ' err :' + JSON.stringify(errKey) + '\n';
        }
      })
        .setCertDataByRes(hereResourceManager, this.certRes, (resulterrKey: Uint8Array) => {
          if (isNeedShow) {
            this.content += '\ntlsSoket:cert:' + JSON.stringify(resulterrKey) + '\n'
          }
        })
        .setCaDataByRes(hereResourceManager, this.caRes, (resultCa: string | boolean | string[]) => {
          if (isNeedShow) {
            this.content += '\ntlsSoket:ca:' + JSON.stringify(resultCa) + '\n'
          }
        })
        .setUseRemoteCipherPrefer(this.ifUseRemoteCipherPrefer)
        .setSignatureAlgorithms(this.currentSignatureAlgorithms)
        .setCipherSuites(this.currentCipherSuites)
        .setPasswd(this.currentPasswd)
        .setProtocols(this.protocols)
        .setALPNProtocols(this.currentALPNProtocols)
    }
  }

  aboutToDisappear() {
    if (this.realTlsSocet != undefined) {
      this.realTlsSocet.close((error: BusinessError<string>) => {
      })
    }
  }

  connect() {
    let that = this
    that.content = ''
    if (StringUtil.isEmpty(this.ipInput)) {
      prompt.showToast({ message: this.getResourceString($r('app.string.Enter_server_IP')) })
      return
    }
    if (StringUtil.isEmpty(this.portInput)) {
      prompt.showToast({ message: this.getResourceString($r('app.string.Enter_port')) })
      return
    }
    if (this.request != undefined) {
      this.request.url = this.getUrl()
      this.request.setData(this.sendStr)
      this.client.newCall(this.request)
        .execute()
        .catch((error: SocketConnectError) => {
          hilog.error(0x0000, this.certificate_cert_TAG, '%{public}s', "connect error : " + JSON.stringify(error));
        })
    }
  }

  getUrl() {
    let url = 'http://' + this.ipInput + ':' + this.portInput
    return url;
  }

  send() {
    this.connect()
  }

  refreshData(): boolean {
    this.content = "";
    this.content += "\n\n " + this.getResourceString($r('app.string.Certificate_verification_result')) + "\n\n" + this.getVerifyDataResult(this.tlsSocketListenerImpl.onVerifyDataResult);
    this.content += "\n\n " + this.getResourceString($r('app.string.IP_result')) + "\n" + JSON.stringify(this.tlsSocketListenerImpl.onBindData);
    this.content += "\n\n " + this.getResourceString($r('app.string.Connection_result')) + "\n" + JSON.stringify(this.tlsSocketListenerImpl.onConnectData);
    this.content += "\n\n " + this.getResourceString($r('app.string.Received_result')) + "\n" + JSON.stringify(this.tlsSocketListenerImpl.onMessageData);
    this.content += "\n\n" + this.tlsSocketListenerImpl.content;
    return true;
  }

  getVerifyDataResult(certVerifyResult: CertVerifyResult[]): string {
    let formatResult: string = "";
    for (let index = 0, len = certVerifyResult.length; index < len; index++) {
      formatResult += "\n" + JSON.stringify(certVerifyResult[index]) + "\n";
    }
    return formatResult;
  }
}

@Component
struct CertVerifyButtonStyle {
  name: ResourceStr = "";
  tlsSocketListenerImpl?: TLSSocketListenerImpl = new TLSSocketListenerImpl();

  build() {
    Button(this.name)
      .height(px2vp(100))
      .fontSize(18)
      .fontColor(0xCCCCCC)
      .align(Alignment.Center)
      .margin(10)
  }
}