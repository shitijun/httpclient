const DataModel = require("../model/DataModel");
const SUCCESS_CODE = 200;

/**
 * basCross-domain settings
 *
 * @param req request
 * @param res response
 */
const baseSetting = (req, res, next) => {
  var token = req.headers["authorization"];
  //设置允许跨域的域名，*代表允许任意域名跨域
  res.header("Access-Control-Allow-Origin", "*");
  //允许的header类型
  res.header("Access-Control-Allow-Headers", "*");
  //跨域允许的请求方式
  res.header(
    "Access-Control-Allow-Methods",
    "DELETE,PUT,POST,GET,OPTIONS,PATCH"
  );
  //让options请求快速返回
  if (req.method === "OPTIONS") res.send(200);
  // else if(req.originalUrl.indexOf('/tpc/') != -1){
  //   token ? next() : res.json(401, { error: 'token认证失败！' })
  // }
  else {
    next();
  }
};

/**
 * search tpc detail.
 *
 * @param req request
 * @param res response
 */
const postTpcDetail = (req, res) => {
  if (req.body.id) {
    let result = DataModel.tpcList.filter((item) => {
      return item.id == req.body.id;
    });
    res.send({ code: SUCCESS_CODE, data: result, msg: "提交成功" });
  } else {
    res.json(500, { error: "参数错误！" });
  }
};

module.exports = {
  baseSetting,
  postTpcDetail,
};
