var express = require("express");
var router = express.Router();
var multer = require("multer");

var TpcController = require("../controllers/tpcController");

var storage = multer.diskStorage({
  //保存路径
  destination: function (req, file, cb) {
    cb(null, "./public/upload");
  },
  //保存在 destination 中的文件名
  filename: function (req, file, cb) {
    const filename = req.body.name
    cb(null, filename)
  },
});
var uploads = multer({ storage: storage });

// 跨域处理
router.all("*", TpcController.baseSetting);

// post请求
router.post("/tpc/post", TpcController.postTpcDetail);

module.exports = router;
