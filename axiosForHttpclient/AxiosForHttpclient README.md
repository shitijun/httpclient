# axiosForHttpclient

## Introduction

[Axios](https://github.com/axios/axios) is a promise-based network request library that can run in Node.js and browsers. axiosForHttpclient is adapted based on [HttpClient](https://gitee.com/openharmony-tpc/httpclient) 2.0.0-rc.4 to ensure compatibility with OpenHarmony, while retaining its original usage and features. In addition to the following features provided by axios, this library also provides extended features such as custom certificates, certificate pinning, custom DNS, and network event listening:

- HTTP request
- Promise APIs
- Request and response data conversion
- Automatic conversion of JSON data

## How to Install

```javascript
ohpm install @ohos/axiosforhttpclient
```

For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## Required Permissions
```
ohos.permission.INTERNET
ohos.permission.GET_NETWORK_INFO
ohos.permission.GET_WIFI_INFO
```

## How to Use

Before using the demo, change the server address in the **Common.ets** file under **entry > src > main > ets > common** in the demo, and add a correct certificate to the **entry > src > main > resources > rawfile** directory.

**Sending a GET Request**

Axios supports generic parameters, but ArkTS does not support the any type. Therefore, you need to specify the parameter type.

Example: axios.get<T = any, R = AxiosResponse<T>, D = any>(url)

- **T**: response data type. When a POST request is sent, the client may receive a JSON object. **T** is the type of this JSON object. By default, **T** is **any**, which means that any type of data can be received.
- **R**: response body type. When the server returns a response, the response body is usually a JSON object. **R** is the type of this JSON object. By default, **R** is **AxiosResponse\<T>**, which means that the response body is an **AxiosResponse** object whose **data** property is of the **T** type.
- **D**: request parameter type. In a GET request, some query parameters may be added to the URL. **D** is the type of these query parameters. If no query parameters are carried, **D** is of the null type.
```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'
interface UserInfo{
  id: number
  name: string,
  phone: number
}

// Send an HTTPS request to a user with the specified ID.
axios.get<UserInfo, AxiosResponse<UserInfo>, null>('https://www.xxx.com/user?ID=12345',
  { 
    caPath: 'xxx.pem',
    context: getContext(this),
  })
.then((response: AxiosResponse<UserInfo>)=> {
  // Handle the success case.
  console.info("id" + response.data.id)
  console.info(JSON.stringify(response));
})
.catch((error: AxiosError)=> {
  // Handle the error case.
  console.info(JSON.stringify(error));
})
.then(()=> {
  // Always executed.
});

// Send an HTTP request to a user with the specified ID.
axios.get<UserInfo, AxiosResponse<UserInfo>, null>('http://www.xxx.com/user?ID=12345',{})
.then((response: AxiosResponse<UserInfo>)=> {
    // Handle the success case.
    console.info("id" + response.data.id)
    console.info(JSON.stringify(response));
})
.catch((error: AxiosError)=> {
    // Handle the error case.
    console.info(JSON.stringify(error));
})
.then(()=> {
    // Always executed.
});

// (Optional) The preceding request can also be completed in the following way:
axios.get<UserInfo, AxiosResponse<UserInfo>, null>('http://www.xxx.com/user', {
  params: {
    ID: 12345
  },
})
.then((response:AxiosResponse<UserInfo>) => {
  console.info("id" + response.data.id)
  console.info(JSON.stringify(response));
})
.catch((error:AxiosError) => {
  console.info(JSON.stringify(error));
})
.then(() => {
  // Always executed.
});

// The preceding HTTP request can also be completed in the following way (optional):
axios.get<UserInfo, AxiosResponse<UserInfo>, null>('https://www.xxx.com/user', {
  params: {
    ID: 12345
  },
  caPath: 'xxx.pem',
  context: getContext(this),
})
.then((response:AxiosResponse<UserInfo>) => {
    console.info("id" + response.data.id)
    console.info(JSON.stringify(response));
})
.catch((error:AxiosError) => {
    console.info(JSON.stringify(error));
})
.then(() => {
    // Always executed.
});

// async/await is supported.
async function getUser() {
  try {
        const response:AxiosResponse = await axios.get<string, AxiosResponse<string>, null>(this.getUrl);
        console.log(JSON.stringify(response));
      } catch (error) {
    console.error(JSON.stringify(error));
  }
}
```

**Sending a POST Request**
```javascript
interface User {
  firstName: string,
  lastName: string
}
   axios.post<string, AxiosResponse<string>, User>('https://www.xxx.com/user', {
     firstName: 'Fred',
     lastName: 'Flintstone'
   },{
      caPath: 'xxx.pem',
      context: getContext(this),
    })
   .then((response: AxiosResponse<string>) => {
     console.info(JSON.stringify(response));
   })
   .catch((error) => {
  console.info(JSON.stringify(error));
});
```

## How to Use

### axios APIs

#### Creating a Request by Transferring Configurations to Axios

##### axios(config)
```javascript
// Send a GET request.
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'

axios<string, AxiosResponse<string>, null>({
  method: "get",
  url: 'https://www.xxx.com/info'
  caPath: 'xxx.pem',
  context: getContext(this),
}).then((res: AxiosResponse) => {
  console.info('result:' + JSON.stringify(res.data));
}).catch((error: AxiosError) => {
  console.error(error.message);
})
```

##### axios(url[, config])
```javascript
// Send a GET request (default request mode).
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'

axios.get<string, AxiosResponse<string>, null>('https://www.xxx.com/info', 
{ 
  caPath: 'xxx.pem',
  context: getContext(this),
  params: { key: "value" } 
})
.then((response: AxiosResponse) => {
  console.info("result:" + JSON.stringify(response.data));
})
.catch((error: AxiosError) => {
  console.error("result:" + error.message);
});
```

#### Creating a Request Using Aliases
For convenience, aliases are provided for all supported request methods.

- axios.get(url[, config])
- axios.delete(url[, config])
- axios.post(url[, data[, config]])
- axios.put(url[, data[, config]])

> **NOTE**
>
> When using aliases, you do not need to specify the **url**, **method**, and **data** properties in **config**. However, you must specify the **caPath** and **context** properties.

### Specifying the Return Value Type
**responseType**: type of the returned data. This parameter is not used by default. If this parameter is set, the system returns the specified type of data preferentially. The options are as follows: **string**, **object**, and **array_buffer**. When this parameter is set, **response.data** is of the specified type.

```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'

 axios<string, AxiosResponse<string>, null>({
    url: 'https://www.xxx.com/info',
    method: 'get',
    responseType: 'array_buffer', 
    caPath:", // CA certificate path. // This parameter is not required for HTTP requests.
    context: getContext(this), // This parameter is not required for HTTP requests.
  }).then((res: AxiosResponse) => {
   // Logic for processing a successful request.
  })
```

### Customizing a CA Certificate

```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'

  axios<InfoModel, AxiosResponse<InfoModel>, null>({
    url: 'https://www.xxx.com/xx',
    method: 'get',
    caPath:", // CA certificate path.
    context: getContext(this),
  }).then((res: AxiosResponse) => {
    // 
  }).catch((err: AxiosError) => {
    //
  })
```

### Verifying a Custom Certificate

```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'
import { X509TrustManager } from '@ohos/axiosforhttpclient'

  axios<InfoModel, AxiosResponse<InfoModel>, null>({
    url: 'https://www.xxx.com/xx',
    method: 'get',
    caPath:", // CA certificate path.
    context: getContext(this),
    sslCertificateManager: new SslCertificateManager(), // Certificate verification.
  }).then((res: AxiosResponse) => {
    // 
  }).catch((err: AxiosError) => {
    //
  })

  export class SslCertificateManager implements X509TrustManager{
    checkServerTrusted(X509Certificate: certFramework.X509Cert): void {
       //
     }
    checkClientTrusted(X509Certificate: certFramework.X509Cert): void {
      //
     }
  }
```

### Customizing a Client Certificate

```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'

  axios<InfoModel, AxiosResponse<InfoModel>, null>({
    url: 'https://www.xxx.com/xx',
    method: 'get',
    caPath:", // CA certificate path.
    clientCert: {
        certPath:", // Client certificate path.
        certType: 'pem', // Client certificate type, which can be pem and crt.
        keyPath:", // Path of the client private key.
        keyPasswd: " // Password.
      },
    context: getContext(this),
  }).then((res: AxiosResponse) => {
    
  }).catch((err: AxiosError) => {
    
  })
```

### Setting a Proxy
```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'

    axios<string, AxiosResponse<string>, null>({
      url: 'http://www.xxx.com',
      method: 'get',
      proxy:{
        host: 'xxx',
        port: xx,
        exclusionList: []
      } 
    }).then((res: AxiosResponse) => {
      
    }).catch((err: AxiosError) => {
      
    })
```

### Setting a DNS
```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'
import { DNS } from '@ohos/httpclient'

    axios<string, AxiosResponse<string>, null>({
      url: 'https://www.xxx.com',
      method: 'get',
      dns: new CustomDns(), // Custom a DNS.
      caPath:", // CA certificate path.
      context: getContext(this),
    }).then((res: AxiosResponse) => {
      
    }).catch((err: AxiosError) => {
      
    })

    export class CustomDns implements Dns {
      lookup(hostname: string): Promise<Array<connection.NetAddress>> {
        // todo
      }
    }
```

### Setting the Priority
```javascript
import { axios, AxiosError, AxiosResponse, HttpClient} from '@ohos/axiosforhttpclient'

	let client = new HttpClient.Builder();
    axios<string, AxiosResponse<string>, null>({
      url: 'https://www.xxx.com',
      method: 'get',
      caPath:", // CA certificate path.
      context: getContext(this),
      client: client,
      priority: 0, // Priority configuration.
    }).then((res: AxiosResponse) => {
      
    }).catch((err: AxiosError) => {
      
    })
```

### Setting an Event Listener
```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'
import { EventListener } from '@ohos/axiosforhttpclient';

    axios<string, AxiosResponse<string>, null>({
      url: 'https://www.xxx.com',
      method: 'get',
      eventListener: new EventTest(),// Event listener.
      caPath: '', 
      context: getContext(this),
    }).then((res: AxiosResponse) => {
      
    }).catch((err: AxiosError) => {
      
    })
    export class EventTest extends EventListener {
      //
}
```
### Setting a Priority Queue
```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'

let client = new HttpClient.Builder();
for (let i = 1; i < 100; i++) {
      axios<string, AxiosResponse<string>, null>({
        url: 'http://www.xxx.com?' + 'id=' + i,
        method: 'get',
        client: client,
        priority: i === 50 ? 20 : 1,  // Set a priority queue. By default, the maximum number of concurrent queues is 64, the maximum number of concurrent queues on a host is 5. The default priority of a queue is 0. A larger value indicates a higher priority.
        async: true // The priority queue takes effect only in asynchronous request mode.
      }).then((res: AxiosResponse) => {
      
      }).catch((err: AxiosError) => {

      })
    }
```

### Setting the Cache
```javascript
import { axios, Cache, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'
let context = getContext();
let hereCacheDir: string = context.cacheDir;
let cache: Cache.Cache = new Cache.Cache(hereCacheDir, 10 * 1024 * 1024, context);
axios<string, AxiosResponse<string>, null>({
      url: "https://www.xxx.com",
      method: 'get',
      context: getContext(this),
      caPath: '',
      cache: cache,
      responseType: 'string'
    }).then((res: AxiosResponse<string>) => {

    }).catch((err: AxiosError) => {

})
```

### Setting a Synchronous or Asynchronous Request

```javascript
axios<string, AxiosResponse<string>, null>({
  url: 'https://www.xxx.com',
  method: 'get',
  context: getContext(this),
  caPath: 'xxx',
  async: true, // If async is not configured or is set to false, the request is a synchronous request. If async is set to true, the request is an asynchronous request.
  sslCertificateManager: new SslCertificateManager(),
}).then((res: AxiosResponse<string>) => {
  this.status = res ? res.status : '';
  this.message = res ? JSON.stringify(res.data) : '';
}).catch((err: AxiosError) => {
  this.status = '';
  this.message = err.message;
})
```

### Pinning a Certificate

The usage of certificate pinning is as follows:

Configure certificate information in the configuration file **entry/src/main/resources/rawfile/network_config.json**.

If the file exists, the system automatically links the host name with the certificate public key hash configured in the file. All requests that do not match the configuration are intercepted.

Configuration file: **network_config**
```json
{
  "network-security-config": {
    "domain-config": [
      {
        "domains": [
          {
            "include-subdomains": true,
            "name": "x.x.x.x" // IP address or domain name
          }
        ],
        "pin-set": {
          "expiration": "2024-8-6," // Validity period of certificate pinning
          "pin": [
            {
              "digest-algorithm": "sha256," // Hash algorithm of the message digest. The supported format is sha256.
              "digest": "WAFcHG6pAINrztx343ccddfzLOdfoDS9pPgMv2XHk=" // Message digest
            }
          ]
        }
      }
    ]
  }
}
```

## Constraints

This project has been verified in the following version:

4.1 Canary (4.1.3.319), OpenHarmony SDK: API 11 (4.1.3.1)

## Features Not Yet Supported

- HTTPS proxy<br>
  The bottom layer of the library depends on the @ohos.net.socket module, which does not support the HTTPS proxy.

- Certificates in .p12 format<br>

  The bottom layer of the library depends on the @ohos.net.socket module, which does not support certificates in .p12 format.

- Using the system CA certificate<br>The bottom layer of the library depends on the @ohos.net.socket module, which cannot use the system CA certificate during network connection. Therefore, you must manually transfer a CA certificate when sending an HTTPS request. In other words, **caPath** and **context** cannot be empty; otherwise, the message "SSL is null" is displayed.

## Usage and Description of Axios
See [Axios](https://gitee.com/openharmony-sig/ohos_axios/blob/master/README_EN.md).

## How to Contribute

If you find any problem when using axiosForHttpclient, submit an [issue](https://gitee.com/openharmony-tpc/httpclient/issues) or [PR](https://gitee.com/openharmony-tpc/httpclient/pulls).

## License

This project is licensed under [MIT License](https://gitee.com/openharmony-tpc/httpclient/blob/master/LICENSE).
