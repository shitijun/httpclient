/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export interface IdModel {
  id?: number
}

interface UrlModel {
  url: string
}

export interface UploadModel {
  code: number,
  msg: string,
  ts: string,
  data: UrlModel
}

export interface InfoModel {
  id: number
  name: string
  briefDesc: string
  author: string
  licence: string
  gitUrl: string
  keyword: string
}

export interface ConfigModel {
  defaultUrl: string,
  path: string,
  getUrl: string,
  postUrl: string,
  downloadUrl: string,
  uploadUrl: string,
}

export interface NormalResultModel {
  code: number,
  data: Object,
  msg: string,
  pageNum: number,
  pageSize: number,
  totalNum: number,
  totalPage: number
}

export interface PostDataModel {
  pageNum: number,
  pageSize: number,
  newDate: number
}

export interface DownLoadResultModel {
  data: string
}


export interface UploadResultModel {
  code: number,
  msg: string,
  ts: string,
  data: UrlModel
}

